#!/usr/bin/env python

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

from abc import ABCMeta, abstractmethod

import numpy as np
import pyquaternion
from geometry_msgs.msg import Point, Vector3, Quaternion, Pose, Transform


class ROSMessageBridge(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_ros_message(self):
        pass

    @classmethod
    def from_msg(cls, msg):
        pass


class Vector3Bridge(ROSMessageBridge, np.ndarray):
    def __new__(cls, array=[0., 0., 0.]):
        return np.ndarray.__new__(cls, (3,))

    def __init__(self, array=[0., 0., 0.]):
        self[:] = array

    def get_ros_message(self):
        return Vector3(*self)

    @classmethod
    def from_msg(cls, msg=Vector3()):
        result = cls()
        result[0] = msg.x
        result[1] = msg.y
        result[2] = msg.z
        return result


class PointBridge(Vector3Bridge):
    def get_ros_message(self):
        return Point(*self)


class QuaternionBridge(ROSMessageBridge, pyquaternion.Quaternion):
    def __init__(self, *args, **kwargs):
        pyquaternion.Quaternion.__init__(self, *args, **kwargs)

    def get_ros_message(self):
        return Quaternion(self[1], self[2], self[3], self[0])

    @classmethod
    def from_msg(cls, msg=Quaternion()):
        result = cls()
        result[0] = msg.w
        result[1] = msg.x
        result[2] = msg.y
        result[3] = msg.z
        return result


class PoseBridge(ROSMessageBridge):
    def __init__(self, position=PointBridge(), orientation=QuaternionBridge()):
        self.position = position
        self.orientation = orientation

    def get_ros_message(self):
        position_msg = self.position.get_ros_message()
        orientation_msg = self.orientation.get_ros_message()
        return Pose(position_msg, orientation_msg)

    @classmethod
    def from_msg(cls, msg):
        result = cls()
        result.position = PointBridge.from_msg(msg.position)
        result.orientation = QuaternionBridge.from_msg(msg.orientation)
        return result


class TransformBridge(ROSMessageBridge):
    def __init__(self, translation=Vector3Bridge(),
                 rotation=QuaternionBridge()):

        self.translation = translation
        self.rotation = rotation

    def get_ros_message(self):
        translation_msg = self.translation.get_ros_message()
        rotation_msg = self.rotation.get_ros_message()
        return Transform(translation_msg, rotation_msg)

    @classmethod
    def from_msg(cls, msg):
        result = cls()
        result.translation = Vector3Bridge.from_msg(msg.translation)
        result.rotation = QuaternionBridge.from_msg(msg.rotation)
        return result

    def get_transform_to(self, target):
        result = TransformBridge()
        result.translation = self.translation - target.translation
        result.translation = target.rotation.inverse.rotate(result.translation)
        result.rotation = target.rotation.inverse * self.rotation
        return result
